// bm_omp.cpp

/* After completing my test,
I requested suggestions from ChatGPT
for improving the code. */

/*This C++ program, bm_omp.cpp, implements the Boyer-Moore string 
searching algorithm with parallel processing using OpenMP. It searches 
for occurrences of a specified pattern in a given text file, measures 
the elapsed time, and outputs the text and pattern lengths 
along with the total matches found.*/

#include <iostream>
#include <vector>
#include <fstream>
#include <sstream>
#include <algorithm>
#include <string>
#include <chrono>
#include <climits>
#include <omp.h>

using namespace std;
using namespace std::chrono;

// Define a vector type for the occurrence table used in the Boyer-Moore algorithm.
typedef vector<size_t> OccTableType;

// Function to create and initialize the occurrence table for the given pattern.
// This table is used in the Boyer-Moore algorithm to determine
// how far to jump in the text when a mismatch occurs.
OccTableType CreateTable(const string& pattern) {
    OccTableType occ(UCHAR_MAX + 1, pattern.size());
    if (!pattern.empty()) {
        for (size_t i = 0; i < pattern.size() - 1; ++i) {
            occ[pattern[i]] = pattern.size() - 1 - i;
        }
    }
    return occ;
}

// Implementation of the Boyer-Moore algorithm for string searching.
// This function searches for occurrences of 'pattern' in 'text' using the provided occurrence table.
// It also takes the number of threads to use for parallel processing.
int BoyerMooreHorspool(const string& text, const string& pattern, const OccTableType& occTable, int numThreads) {
    int count = 0;  // Counter for the number of matches found
    int blockSize = text.size() / numThreads;  // Calculate block size for each thread
    int overlap = pattern.size() - 1;  // Overlap between blocks to avoid missing matches

    // Parallel section using OpenMP. Each thread processes a separate block of the text.
    #pragma omp parallel num_threads(numThreads) reduction(+:count)
    {
        int tid = omp_get_thread_num();  // Thread ID
        size_t start = tid * blockSize;  // Start index for this thread
        size_t end = (tid == numThreads - 1) ? text.size() : (tid + 1) * blockSize + overlap;  // End index for this thread

        // Search for the pattern in the assigned block of text
        for (size_t pos = start; pos < end && pos + pattern.size() <= text.size(); pos += occTable[text[pos + pattern.size() - 1]]) {
            if (text.substr(pos, pattern.size()) == pattern) {
                count++;  // Increment count if a match is found
            }
        }
    }

    return count;  // Return the total number of matches found
}

// Main function: Entry point of the program
int main(int argc, char *argv[]) {
    // Check for correct command-line arguments
    if (argc < 4) {
        cerr << "Usage: " << argv[0] << " <filename> <pattern> <num_threads>" << endl;
        return 1;
    }

    // Open the file containing the text to search
    ifstream inFile(argv[1]);
    if (!inFile) {
        cerr << "Error opening file: " << argv[1] << endl;
        return 1;
    }

    // Read the contents of the file into a string
    stringstream buffer;
    buffer << inFile.rdbuf();
    string text = buffer.str();

    // The pattern to search for is taken from the command line
    string pattern = argv[2];
    // Number of threads for parallel processing is taken from the command line
    int numThreads = stoi(argv[3]);

    // Create the occurrence table for the pattern
    OccTableType occTable = CreateTable(pattern);

    // Measure the time taken by the Boyer-Moore algorithm
    auto start = high_resolution_clock::now();
    int matches = BoyerMooreHorspool(text, pattern, occTable, numThreads);
    auto stop = high_resolution_clock::now();

    // Calculate the duration of the search
    auto duration = duration_cast<microseconds>(stop - start);

    // Display the results
    cout << "Parallel BM Algorithm Results\n";
    cout << "Text Length: " << text.size() << endl;
    cout << "Pattern Length: " << pattern.size() << endl;
    cout << "Elapsed Time: " << duration.count() / 1000.0 << " ms\n";
    cout << "Total Matches Found: " << matches << endl;

    return 0;
}




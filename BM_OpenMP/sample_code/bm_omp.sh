#!/bin/bash

# SLURM script for running the Boyer-Moore-Horspool algorithm in parallel using OpenMP

#SBATCH --job-name=bm_omp                  # Job name: 'bm_omp' to reflect the Boyer-Moore algorithm with OpenMP
#SBATCH --partition=instruction            # Specify the partition or queue
#SBATCH --time=00-00:30:00                 # Runtime in Days-Hours:Minutes:Seconds (30 minutes here)
#SBATCH --ntasks=1                         # Number of tasks (jobs), typically 1 for serial jobs
#SBATCH --cpus-per-task=4                  # Number of cores per task (4 in this case)
#SBATCH --mem=20G                          # Memory needed per job (20 gigabytes here)
#SBATCH --output=bm_omp.out                # Name of the output file (stdout and stderr)

cd $SLURM_SUBMIT_DIR                       # Change directory to the directory where the job was submitted

# Compile the C++ program with OpenMP support
# 'bm_omp.cpp' is the name of the C++ source file
# The '-Wall' flag enables all compiler's warning messages
# The '-O3' flag enables optimizations
# The '-std=c++17' flag specifies the C++ standard to use (C++17 in this case)
# The '-o bm_omp' specifies the output executable name 'bm_omp'
# The '-fopenmp' flag is used to enable OpenMP support for parallel execution
g++ bm_omp.cpp -Wall -O3 -std=c++17 -o bm_omp -fopenmp

# Execute the compiled program with two arguments:
# The first argument is the name of the text file to be searched ('target.txt')
# The second argument is the pattern to search for in the text ('target')
./bm_omp target.txt target





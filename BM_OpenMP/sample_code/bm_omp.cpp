// bm_omp.cpp

/* After completing my test,
   I requested suggestions from ChatGPT
   for improving the code. */

/* This C++ program employs the Boyer-Moore string searching algorithm 
with parallel processing using OpenMP. It searches for occurrences of a 
specified pattern within a given text file, measures the elapsed time, 
and reports the text and pattern lengths along with the total number of matches found.*/

#include <iostream>
#include <vector>
#include <fstream>
#include <sstream>
#include <algorithm>
#include <string>
#include <chrono>
#include <climits>
#include <omp.h>

using namespace std;
using namespace std::chrono;

// OccTableType: A vector that holds the occurrence table for the Boyer-Moore algorithm.
typedef vector<size_t> OccTableType;

// CreateTable: Function to create and initialize the occurrence table for the pattern.
// The table determines how far to jump ahead when a mismatch is found.
OccTableType CreateTable(const string& pattern) {
    OccTableType occ(UCHAR_MAX + 1, pattern.size());
    if (!pattern.empty()) {
        for (size_t i = 0; i < pattern.size() - 1; ++i) {
            occ[pattern[i]] = pattern.size() - 1 - i;
        }
    }
    return occ;
}

// BoyerMooreHorspool: The main function implementing the Boyer-Moore algorithm.
// This function searches for occurrences of 'pattern' in 'text'.
int BoyerMooreHorspool(const string& text, const string& pattern, const OccTableType& occTable) {
    int count = 0; // Counter for the number of pattern matches
    int blockSize = text.size() / omp_get_max_threads(); // Dividing the text into blocks for parallel processing
    int overlap = pattern.size() - 1; // Overlap to handle edge cases in parallel blocks

    // Parallel section using OpenMP. Each thread processes a block of text.
    #pragma omp parallel num_threads(omp_get_max_threads()) reduction(+:count)
    {
        int tid = omp_get_thread_num();
        size_t start = tid * blockSize;
        size_t end = (tid == omp_get_max_threads() - 1) ? text.size() : (tid + 1) * blockSize + overlap;

        // Searching for the pattern in the assigned block of text.
        for (size_t pos = start; pos < end && pos + pattern.size() <= text.size(); pos += occTable[text[pos + pattern.size() - 1]]) {
            if (text.substr(pos, pattern.size()) == pattern) {
                count++; // Increment count if pattern is found
            }
        }
    }

    return count; // Return the total count of pattern occurrences
}

// main: The entry point of the program
int main(int argc, char *argv[]) {
    // Checking for correct command-line arguments
    if (argc < 3) {
        cerr << "Usage: " << argv[0] << " <filename> <pattern>" << endl;
        return 1;
    }

    // Opening the file containing the text
    ifstream inFile(argv[1]);
    if (!inFile) {
        cerr << "Error opening file: " << argv[1] << endl;
        return 1;
    }

    // Reading the contents of the file into a string
    stringstream buffer;
    buffer << inFile.rdbuf();
    string text = buffer.str();

    // The pattern to search for is taken from the command line argument
    string pattern = argv[2];
    // Creating the occurrence table for the pattern
    OccTableType occTable = CreateTable(pattern);

    // Measuring the time taken by the Boyer-Moore-Horspool algorithm
    auto start = high_resolution_clock::now();
    int matches = BoyerMooreHorspool(text, pattern, occTable);
    auto stop = high_resolution_clock::now();

    // Calculating the duration of the search
    auto duration = duration_cast<microseconds>(stop - start);

    // Displaying the results
    cout << "Parallel BM Algorithm Results\n";
    cout << "Text Length: " << text.size() << endl;
    cout << "Pattern Length: " << pattern.size() << endl;
    cout << "Elapsed Time: " << duration.count() / 1000.0 << " ms\n";
    cout << "Total Matches Found: " << matches << endl;

    return 0;
}





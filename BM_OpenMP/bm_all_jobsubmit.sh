#!/bin/bash

# Loop through numbers from 1 to 10
for n in $(seq 1 10)  
do 
    # Create a directory named after the current number (n)
    DIR="${n}"
    mkdir $DIR

    # Copy the necessary files into the newly created directory
    # This includes the bm_omp.sh script, the C++ source file bm_omp.cpp, and a test file
    cp bm_omp.sh bm_omp.cpp target.txt $DIR

    # Change the current working directory to the one just created
    cd $DIR

    # Use sed to replace placeholders {n} in bm_omp.sh with the current number (n)
    # This process is done in two steps using temporary files:
    # 1. Replace {n} in bm_omp.sh and output to a temporary file (tmp1)
    # 2. Replace {n} in tmp1 and output to another temporary file (tmp2)
    sed 's/{n}/'"$n"'/g' < bm_omp.sh > tmp1
    sed 's/{n}/'"$n"'/g' < tmp1 > tmp2

    # Rename the final modified file back to bm_omp.sh
    # Delete the temporary files used for the intermediate steps
    mv tmp2 bm_omp.sh
    rm tmp*

    # Submit the modified job script to the SLURM scheduler
    # This will queue the job for execution on the cluster
    sbatch bm_omp.sh

    # Change back to the parent directory after the job submission
    cd ..
done  # End of the loop over the numbers 1 to 10





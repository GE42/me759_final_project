#!/bin/bash

#SBATCH --job-name=bm_omp         # Job name for the SLURM job
#SBATCH --partition=instruction   # Specify the job partition or queue
#SBATCH --time=00-00:30:00        # Maximum runtime for the job (30 minutes)
#SBATCH --ntasks=1               # Number of tasks (jobs) to run (typically 1 for serial jobs)
#SBATCH --cpus-per-task=4        # Number of CPU cores per task (4 in this case)
#SBATCH --mem=20G                # Memory needed per job (20 gigabytes)
#SBATCH --output=bm_omp.out      # Name of the output file (stdout and stderr)

cd $SLURM_SUBMIT_DIR              # Change the working directory to where the job was submitted

# Compile the C++ program with OpenMP support
g++ bm_omp.cpp -Wall -O3 -std=c++17 -o bm_omp -fopenmp

# Execute the compiled program with three arguments:
# 1. Filename (e.g., target.txt)
# 2. Pattern (e.g., target)
# 3. Number of threads (represented by '{n}' as a placeholder, should be replaced with the actual number)
./bm_omp target.txt target {n}






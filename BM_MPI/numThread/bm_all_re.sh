#!/bin/bash

# This script aggregates elapsed time information from multiple output files.

# Check if a file named "result.txt" exists in the current directory
if [ -f "result.txt" ]; then
    # Delete the file if it exists to ensure a fresh start
    rm "result.txt"
fi

# Create a new, empty file named "result.txt" for aggregating results
touch "result.txt"

# Loop through numbers from 1 to 10, representing different test cases or configurations
for n in $(seq 1 10)  
do 
    # Define the directory name based on the current number
    DIR="${n}"

    # Move into the directory if it exists
    cd "$DIR"

    # Extract the elapsed time from the 'bm_mpi.out' file
    # 'sed -n '4p'' is used to select the fourth line of the file
    # 'grep -oP' extracts the numeric value of the elapsed time using a Perl-compatible regex
    # Append the extracted elapsed time (in milliseconds) to "result.txt" in the specified directory
    sed -n '4p' bm_mpi.out | grep -oP '(?<=Elapsed Time: ).*(?= ms)' >> "../result.txt"

    # Change back to the parent directory to process the next directory
    cd ..

done  # End of the loop over the range of numbers from 1 to 10





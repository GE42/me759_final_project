// bm_mpi.cpp

/* This C++ code implements the Boyer-Moore string search algorithm in a parallel
 * and distributed manner using MPI and OpenMP. It searches for a user-provided pattern
 * within a target file, measures the algorithm's execution time, and reports
 * the total number of pattern matches found in the file. The code utilizes
 * multiple threads and processes for efficient pattern searching in large text files.
 */

#include <iostream>
#include <vector>
#include <fstream>
#include <sstream>
#include <cstring>
#include <climits>
#include <cstdlib>
#include <mpi.h>
#include <omp.h>
#include <chrono>

using namespace std;
using namespace std::chrono;

#define NUM_THREADS {n}  // Define the number of threads for OpenMP

typedef vector<size_t> OccurrenceTableType;

// Function to create the occurrence table for Boyer-Moore
OccurrenceTableType CreateOccurrenceTable(const unsigned char* pattern, size_t patternLength) {
    OccurrenceTableType occTable(UCHAR_MAX + 1, patternLength);
    if (patternLength >= 1) {
        for (size_t i = 0; i < (patternLength - 1); ++i) {
            occTable[pattern[i]] = (patternLength - 1) - i;
        }
    }
    return occTable;
}

int main(int argc, char* argv[]) {
    if (argc < 3) {
        cerr << "Usage: " << argv[0] << " <filename> <pattern>" << endl;
        return 1;
    }

    char* filename = argv[1];
    const char* pattern = argv[2];
    size_t patternLength = strlen(pattern);

    MPI_Init(&argc, &argv);
    int processRank, numProcesses;
    MPI_Comm_size(MPI_COMM_WORLD, &numProcesses);
    MPI_Comm_rank(MPI_COMM_WORLD, &processRank);

    MPI_File mpiFile;
    int ierr = MPI_File_open(MPI_COMM_WORLD, filename, MPI_MODE_RDONLY, MPI_INFO_NULL, &mpiFile);
    if (ierr) {
        if (processRank == 0)
            cerr << "Failed to open file: " << filename << endl;
        MPI_Finalize();
        return 2;
    }

    MPI_Offset fileSize, blockSize;
    MPI_File_get_size(mpiFile, &fileSize);
    blockSize = fileSize / numProcesses;

    char* block = new char[blockSize + 1];
    MPI_File_read_at_all(mpiFile, processRank * blockSize, block, blockSize, MPI_CHAR, MPI_STATUS_IGNORE);
    block[blockSize] = '\0';

    OccurrenceTableType occTable = CreateOccurrenceTable(reinterpret_cast<const unsigned char*>(pattern), patternLength);

    auto startTime = high_resolution_clock::now();

    int localCount = 0, totalCount = 0;
    #pragma omp parallel num_threads(NUM_THREADS) reduction(+:localCount)
    {
        int threadId = omp_get_thread_num();
        size_t blockStart = threadId * (blockSize / NUM_THREADS);
        size_t blockEnd = (threadId == NUM_THREADS - 1) ? blockSize : (threadId + 1) * (blockSize / NUM_THREADS);

        for (size_t pos = blockStart; pos < blockEnd && pos + patternLength <= static_cast<size_t>(blockSize); pos++) {
            if (block[pos + patternLength - 1] == pattern[patternLength - 1] && memcmp(pattern, block + pos, patternLength - 1) == 0) {
                localCount++;
            }
        }
    }

    MPI_Reduce(&localCount, &totalCount, 1, MPI_INT, MPI_SUM, 0, MPI_COMM_WORLD);

    auto endTime = high_resolution_clock::now();
    auto duration = duration_cast<microseconds>(endTime - startTime).count() / 1000.0;

    if (processRank == 0) {
        cout << "Parallel BM Algorithm Results\n";
        cout << "Text Length: " << fileSize << endl;
        cout << "Pattern Length: " << patternLength << endl;
        cout << "Elapsed Time: " << duration << " ms\n";
        cout << "Total Matches Found: " << totalCount << endl;
    }

    delete[] block;
    MPI_File_close(&mpiFile);
    MPI_Finalize();
    return 0;
}






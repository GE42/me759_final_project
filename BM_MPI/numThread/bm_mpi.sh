#!/bin/bash

# SLURM script for running the Boyer-Moore-Horspool algorithm with MPI and OpenMP

#SBATCH --job-name=bm_mpi                  # Job name
#SBATCH --partition=instruction            # Queue or partition name
#SBATCH --nodes=1                          # Number of nodes
#SBATCH --ntasks=1                         # Number of MPI tasks
#SBATCH --cpus-per-task=4                  # Number of CPU cores per MPI task
#SBATCH --time=00-10:00                    # Maximum runtime (10 minutes)
#SBATCH --mem=20G                          # Memory allocation per job
#SBATCH --output=bm_mpi.out                # Output file name

cd $SLURM_SUBMIT_DIR                       # Change to the job submission directory

module load mpi/mpich/4.0.2                # Load the MPI module

mpicxx bm_mpi.cpp -Wall -O3 -std=c++17 -o bm_mpi -fopenmp -lgomp

# Run the compiled program using srun with a specified number of tasks
srun -n 2 --cpu-bind=none ./bm_mpi target.txt target





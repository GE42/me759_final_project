// bm_mpi.cpp

/* After completing my test,
I requested suggestions from ChatGPT
for improving the code. */

/* 
This C++ program implements the Boyer-Moore string searching 
algorithm with MPI parallelism. It searches for occurrences of a 
specified pattern within a given text file, measures the elapsed time, 
and reports the text and pattern lengths, along with the total 
number of matches found across multiple MPI processes.*/

#include <iostream>
#include <vector>
#include <fstream>
#include <sstream>
#include <cstring>
#include <climits>
#include <cstdlib>
#include <mpi.h>
#include <omp.h>
#include <chrono>

using namespace std;
using namespace std::chrono;

#define NUM_THREADS 1  // Define the number of threads for OpenMP

typedef vector<size_t> OccurrenceTableType;

// Function to create an occurrence table for Boyer-Moore algorithm
OccurrenceTableType CreateOccurrenceTable(const unsigned char* pattern, size_t patternLength) {
    OccurrenceTableType occTable(UCHAR_MAX + 1, patternLength);
    if (patternLength >= 1) {
        // Initialize occurrence table with default values
        for (size_t i = 0; i < (patternLength - 1); ++i) {
            occTable[pattern[i]] = (patternLength - 1) - i;
        }
    }
    return occTable;
}

int main(int argc, char* argv[]) {
    if (argc < 3) {
        cerr << "Usage: " << argv[0] << " <filename> <pattern>" << endl;
        return 1;
    }

    char* filename = argv[1];
    const char* pattern = argv[2];
    size_t patternLength = strlen(pattern);

    // Initialize MPI
    MPI_Init(&argc, &argv);
    int processRank, numProcesses;
    MPI_Comm_size(MPI_COMM_WORLD, &numProcesses);
    MPI_Comm_rank(MPI_COMM_WORLD, &processRank);

    MPI_File mpiFile;
    int ierr = MPI_File_open(MPI_COMM_WORLD, filename, MPI_MODE_RDONLY, MPI_INFO_NULL, &mpiFile);
    if (ierr) {
        // Check for file open failure and handle it
        if (processRank == 0)
            cerr << "Failed to open file: " << filename << endl;
        MPI_Finalize();
        return 2;
    }

    MPI_Offset fileSize, blockSize;
    MPI_File_get_size(mpiFile, &fileSize);
    blockSize = fileSize / numProcesses;

    char* block = new char[blockSize + 1];
    MPI_File_read_at_all(mpiFile, processRank * blockSize, block, blockSize, MPI_CHAR, MPI_STATUS_IGNORE);
    block[blockSize] = '\0';

    OccurrenceTableType occTable = CreateOccurrenceTable(reinterpret_cast<const unsigned char*>(pattern), patternLength);

    auto startTime = high_resolution_clock::now();

    int localCount = 0, totalCount = 0;
    // OpenMP parallelization for pattern search
    #pragma omp parallel num_threads(NUM_THREADS) reduction(+:localCount)
    {
        int threadId = omp_get_thread_num();
        size_t blockStart = threadId * (blockSize / NUM_THREADS);
        size_t blockEnd = (threadId == NUM_THREADS - 1) ? blockSize : (threadId + 1) * (blockSize / NUM_THREADS);

        for (size_t pos = blockStart; pos < blockEnd && pos + patternLength <= static_cast<size_t>(blockSize); pos++) {
            // Check for pattern match and update local count
            if (block[pos + patternLength - 1] == pattern[patternLength - 1] && memcmp(pattern, block + pos, patternLength - 1) == 0) {
                localCount++;
            }
        }
    }

    // Reduce local counts to obtain total count across processes
    MPI_Reduce(&localCount, &totalCount, 1, MPI_INT, MPI_SUM, 0, MPI_COMM_WORLD);

    auto endTime = high_resolution_clock::now();
    auto duration = duration_cast<microseconds>(endTime - startTime).count() / 1000.0;

    if (processRank == 0) {
        // Output the results for the root process
        cout << "Parallel BM Algorithm Results\n";
        cout << "Text Length: " << fileSize << endl;
        cout << "Pattern Length: " << patternLength << endl;
        cout << "Elapsed Time: " << duration << " ms\n";
        cout << "Total Matches Found: " << totalCount << endl;
    }

    // Clean up and finalize MPI
    delete[] block;
    MPI_File_close(&mpiFile);
    MPI_Finalize();
    return 0;
}






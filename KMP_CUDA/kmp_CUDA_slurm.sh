#!/usr/bin/env zsh
#SBATCH --job-name=task1_job
#SBATCH --partition=instruction
#SBATCH --ntasks=1 --cpus-per-task=2
#SBATCH --gres=gpu:1
#SBATCH --mem=10G
#SBATCH --time=00:10:00

# Load the CUDA module
module load nvidia/cuda/11.8.0

# Compile the CUDA program
#nvcc kmp_CUDA_Global.cu -Xcompiler -O3 -Xcompiler -Wall -Xptxas -O3 -std c++17 -o kmp_cuda
nvcc kmp_CUDA_Shared.cu -Xcompiler -O3 -Xcompiler -Wall -Xptxas -O3 -std c++17 -o kmp_cuda


for exp in {1..10}; do
	n=$((2 ** exp))
	./kmp_cuda target $n
done

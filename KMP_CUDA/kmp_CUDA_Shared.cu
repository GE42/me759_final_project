#include <ctime>
#include <iostream>
#include <string>
#include <cstring>
#include <fstream>
#include <sstream>

using namespace std;

// CUDA kernel for the Knuth-Morris-Pratt (KMP) string matching algorithm
__global__ void KMP(const char* __restrict__ pattern,const char* __restrict__ target,const int* __restrict__ failure, int answer[], int pattern_length, int target_length) {
    extern __shared__ char sharedPattern[]; // Declare shared memory for the pattern
    if(blockDim.x<pattern_length){
        int i = pattern_length/blockDim.x;
        int remaining = pattern_length%blockDim.x;
        for (int j =0; j<i ; j++){
            sharedPattern[threadIdx.x+j*blockDim.x] = pattern[threadIdx.x+j*blockDim.x];
        }
        if(threadIdx.x < remaining){
            sharedPattern[threadIdx.x+i*blockDim.x] = pattern[threadIdx.x+i*blockDim.x];
        }
    }
    if(threadIdx.x < pattern_length){
        sharedPattern[threadIdx.x] = pattern[threadIdx.x];
    }
    __syncthreads();

    int index = blockIdx.x * blockDim.x + threadIdx.x;
    int start = pattern_length * index;
    int end = pattern_length * (index + 2) - 1;
    if (start > target_length)
        return;
    if (end > target_length)
        end = target_length;
    int k = 0;
    while (start < end) {
        if (k == -1) {
            start++;
            k = 0;
        } else if (target[start] == sharedPattern[k]) {
            start++;
            k++;
            if (k == pattern_length) {
                answer[start - pattern_length] = start - pattern_length;
                start = start - k + 1;
            }
        } else
            k = failure[k];
    }
}

// Function to preprocess the pattern and populate the failure array
void preKMP(char *pattern, int failure[]) {
    int m = strlen(pattern);
    int k;

    failure[0] = -1;

    for (int i = 1; i < m; i++) {
        k = failure[i - 1];

        while (k >= 0) {
            if (pattern[k] == pattern[i - 1])
                break;
            else
                k = failure[k];
        }

        failure[i] = k + 1;
    }
}

int main(int argc, char *argv[]) {
    // Check for correct number of command-line arguments
    if (argc != 3) {
        cout << "Incorrect number of arguments. Usage: <program> <pattern> <threads_per_block>" << endl;
        return -1;
    }

    // Attempt to open the target text file
    ifstream test_file("../target.txt");
    if (!test_file.is_open()) {
        cout << "Failed to open target file." << endl;
        return -1;
    }

    // Read the contents of the file into a string
    stringstream test_stream;
    test_stream << test_file.rdbuf();
    string test_string = test_stream.str();
    int target_length = test_string.length();
    char* target = new char[target_length + 1];
    strcpy(target, test_string.c_str());

    // Process command-line arguments
    char* pattern = argv[1];
    int threads_per_block = atoi(argv[2]);
    int pattern_length = strlen(pattern);

    // Initialize arrays for the KMP algorithm
    int *failure = new int[pattern_length];
    int *answer = new int[target_length];
    memset(answer, -1, target_length * sizeof(int));

    preKMP(pattern, failure);

    // Allocate memory on the GPU
    char *device_target, *device_pattern;
    int *device_failure, *device_answer;
    cudaMalloc((void **)&device_target, target_length * sizeof(char));
    cudaMalloc((void **)&device_pattern, pattern_length * sizeof(char));
    cudaMalloc((void **)&device_failure, pattern_length * sizeof(int));
    cudaMalloc((void **)&device_answer, target_length * sizeof(int));

    // Copy data from host to device
    cudaMemcpy(device_target, target, target_length * sizeof(char), cudaMemcpyHostToDevice);
    cudaMemcpy(device_pattern, pattern, pattern_length * sizeof(char), cudaMemcpyHostToDevice);
    cudaMemcpy(device_failure, failure, pattern_length * sizeof(int), cudaMemcpyHostToDevice);
    cudaMemcpy(device_answer, answer, target_length * sizeof(int), cudaMemcpyHostToDevice);

    // Set up CUDA events for timing
    cudaEvent_t start, stop;
    cudaEventCreate(&start);
    cudaEventCreate(&stop);

    // Launch the KMP kernel and time its execution
    int block_number = target_length / pattern_length / threads_per_block + 1;
    cudaEventRecord(start);
    KMP<<<block_number, threads_per_block, pattern_length * sizeof(char)>>>(device_pattern, device_target, device_failure, device_answer, pattern_length, target_length);
    cudaEventRecord(stop);
    cudaEventSynchronize(stop);

    // Calculate and display elapsed time
    float ms;
    cudaEventElapsedTime(&ms, start, stop);
    cudaMemcpy(answer, device_answer, target_length * sizeof(int), cudaMemcpyDeviceToHost);

    // Count the number of matches found
    int counter = 0;
    for (int i = 0; i < target_length; i++) {
        if (answer[i] != -1) {
            counter++;
        }
    }

    // Display results
    cout << "KMP_CUDA algorithm(Shared Memory) result: (Target length; Pattern length; Number of patterns found; Time used(ms)" << endl;
    cout << target_length << endl;
    cout << pattern_length << endl;
    cout << counter << endl;
    cout << ms <<endl;

    // Free memory and exit
    cudaFree(device_target);
    cudaFree(device_pattern);
    cudaFree(device_failure);
    cudaFree(device_answer);
    delete[] target;
    delete[] failure;
    delete[] answer;

    return 0;
}


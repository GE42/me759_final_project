// bm_cpu.cpp

/* After completing my testing, I requested suggestions from ChatGPT
   for code improvement. */

/* This C++ code implements the Boyer-Moore string search algorithm to
   find all occurrences of a user-provided pattern within a target text file.
   It does so by preprocessing the pattern to create a bad character heuristic array
   for efficient character comparisons. The code reads the target text from a file,
   measures the algorithm's execution time, and outputs the total number of
   pattern matches found in the text. */

#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
#include <ctime>
#include <vector>
#include <cstring>

using namespace std;

// Preprocessing function for the bad character heuristic of the Boyer-Moore algorithm
void badCharHeuristic(const string &str, int size, int badchar[256]) {
    // Initialize all entries of the badchar array as -1
    fill_n(badchar, 256, -1);

    // Fill the actual value of the last occurrence of each character
    for (int i = 0; i < size; i++) {
        badchar[(int)str[i]] = i;  // Cast to int to handle the character as an ASCII value
    }
}

// Boyer-Moore String Search Algorithm
void BoyerMooreSearch(char *text, char *pattern, int *matchLocations) {
    int m = strlen(pattern);  // Length of the pattern
    int n = strlen(text);     // Length of the text
    int badchar[256];         // The bad character heuristic array

    // Preprocess the pattern and populate the bad character heuristic array
    badCharHeuristic(pattern, m, badchar);

    int s = 0;  // s is the shift of the pattern with respect to the text
    // Loop to find all occurrences of the pattern in the text
    while (s <= (n - m)) {
        int j = m - 1;

        // Keep reducing index j of the pattern while characters of the pattern and text are matching at this shift s
        while (j >= 0 && pattern[j] == text[s + j]) {
            j--;
        }

        // If the pattern is found at the current shift, mark the match position
        if (j < 0) {
            matchLocations[s] = 1;  // Mark the match position
            // Shift the pattern so that the next character in the text aligns with the last occurrence in the pattern
            s += (s + m < n) ? m - badchar[(unsigned char)text[s + m]] : 1;
        } else {
            // Shift the pattern so that the bad character in the text aligns with the last occurrence in the pattern
            s += max(1, j - badchar[(unsigned char)text[s + j]]);
        }
    }
}

int main(int argc, char **argv) {
    // Check if the pattern argument is provided
    if (argc < 2) {
        cerr << "Usage: " << argv[0] << " <pattern>" << endl;
        return 1;  // Exit if no pattern is provided
    }

    // Open and read the text file into a string
    ifstream textFileStream("target.txt");
    stringstream textStringStream;
    textStringStream << textFileStream.rdbuf();  // Read the file buffer into the string stream
    string textContent = textStringStream.str();  // Convert the string stream into a string

    // Use the command-line argument as the pattern string
    string patternContent = argv[1];

    // Allocate dynamic memory for the text and pattern as C-style strings
    char *text = new char[textContent.length() + 1];
    char *pattern = new char[patternContent.length() + 1];

    // Copy the string contents into C-style strings
    strcpy(text, textContent.c_str());
    strcpy(pattern, patternContent.c_str());

    // Array to store the locations of pattern matches in the text
    int *matchLocations = new int[textContent.length()]();  // Initialize with zeros

    // Variables for timing the algorithm
    timespec startTime, endTime;
    double elapsedTime;

    // Start timing the algorithm
    cout << "Sequential BM Algorithm Results" << endl;
    clock_gettime(CLOCK_MONOTONIC, &startTime);

    // Perform the Boyer-Moore string matching algorithm
    BoyerMooreSearch(text, pattern, matchLocations);

    // Stop timing and calculate the elapsed time
    clock_gettime(CLOCK_MONOTONIC, &endTime);
    elapsedTime = (endTime.tv_sec - startTime.tv_sec) * 1e3 + (endTime.tv_nsec - startTime.tv_nsec) / 1e6;

    // Output the results including text and pattern lengths, and elapsed time
    cout << "Text Length: " << textContent.length() << endl;
    cout << "Pattern Length: " << patternContent.length() << endl;
    cout << "Elapsed Time: " << elapsedTime << " ms" << endl;

    // Count and display the total number of pattern matches found in the text
    size_t totalMatches = 0;
    for (size_t i = 0; i < textContent.length(); i++) {
        if (matchLocations[i]) {
            totalMatches++;
        }
    }

    cout << "Total Matches Found: " << totalMatches << endl;

    // Release the dynamically allocated memory
    delete[] text;
    delete[] pattern;
    delete[] matchLocations;

    return 0;
}





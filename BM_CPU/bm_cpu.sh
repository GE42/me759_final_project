#!/bin/bash

#SBATCH --job-name=bm_cpu                  # Job name: Descriptive name for the job, here representing the Boyer-Moore algorithm
#SBATCH --partition=instruction             # Specify the job partition
#SBATCH --time=00-00:30:00                 # Runtime in D-HH:MM:SS format
#SBATCH --ntasks=1                         # Number of tasks (jobs)
#SBATCH --cpus-per-task=4                  # Number of CPU cores per task
#SBATCH --mem=20G                          # Amount of memory needed per job
#SBATCH --output=bm_cpu.out                # Change the output file name to 'bm.out'

cd $SLURM_SUBMIT_DIR                       # Change the working directory to the job submission directory

# Compile the C++ program with OpenMP support
# Ensure that the file name 'bm_cpu.cpp' matches the C++ source file
# The output executable will be named 'bm_cpu'
g++ bm_cpu.cpp -Wall -O3 -std=c++17 -o bm_cpu -fopenmp

# Execute the compiled program
# The argument 'target' is passed to the program as the pattern to search
./bm_cpu target






#!/bin/bash

# SLURM script for running the Knuth-Morris-Pratt (KMP) CPU-based algorithm

#SBATCH --job-name=kmp_cpu           # Job name: 'kmp_cpu' to reflect the KMP CPU algorithm
#SBATCH --partition=instruction      # Specify the partition or queue
#SBATCH --time=00-00:30:00           # Runtime in Days-Hours:Minutes:Seconds (30 minutes here)
#SBATCH --ntasks=1                   # Number of tasks (jobs), typically 1 for serial jobs
#SBATCH --cpus-per-task=1            # Number of CPU cores per task (1 in this case)
#SBATCH --mem=20G                    # Memory needed per job (20 gigabytes here)
#SBATCH --output=kmp_cpu.out         # Name of the output file (stdout and stderr)

cd $SLURM_SUBMIT_DIR                 # Change directory to the directory where the job was submitted

# Compile your C++ program without OpenMP support
g++ kmp_cpu.cpp -Wall -O3 -std=c++17 -o kmp_cpu

# Execute your program with the specified input (target file or pattern)
./kmp_cpu target





/*
 After finishing the code, I use Chat GPT to enhance the readability of my code by adding comments, improving variable naming, structuring the code for better readability, and segmenting the main function for clarity.
 */


#include <mpi.h>
#include <chrono>
#include <stdlib.h>
#include <string>
#include <string.h>
#include <fstream>
#include <sstream>
#include <iostream>

#define MASTER 0 // Define Master process

using namespace std;

// Function declarations
int *kmp(char *target, char *pattern, int *table);
int *preKMP(char *pattern, int len);


// preKMP: Function to create KMP (Knuth-Morris-Pratt) match table
// pattern: the pattern string
// len: length of the pattern string
int *preKMP(char *pattern, int len)
{
    int k = 0;
    int i = 1;
    int *table = (int *)malloc(len * sizeof(int));
    table[0] = k;

    // Fill the table with KMP partial match values
    for (i = 1; i < len; i++)
    {
        while (k > 0 && pattern[i - 1] != pattern[i])
        {
            table[i] = k - 1;
            k = table[i];
        }
        if (pattern[i] == pattern[k])
            k++;
        table[i] = k;
    }
    return table;
}


// kmp: KMP search algorithm implementation
// target: the target string where the pattern is searched
// pattern: the pattern string to search
// table: the KMP table precomputed for the pattern
int *kmp(char *target, char *pattern, int *table)
{
    int n = strlen(target);
    int m = strlen(pattern);
    int *answer = (int *)calloc(n, sizeof(int));
    int j = 0;
    int i = 0;

    // Search for the pattern in the target string
    while (i < n)
    {
        if (pattern[j] == target[i])
        {
            j++;
            i++;
        }
        if (j == m)
        {
            answer[i - j] = 1;
            j = table[j - 1];
        }
        else if (i < n && pattern[j] != target[i])
        {
            if (j != 0)
                j = table[j - 1];
            else
                i++;
        }
    }
    return answer;
}

// mergePartialResultsIntoMain: Helper function to merge partial results into the main result array
// result: pointer to the main result array
// result_start_idx: start index in the main result array
// partial_res: pointer to the partial result array
// partial_size: size of the partial result array
void mergePartialResultsIntoMain(int *result, int result_start_idx, int *partial_res, int partial_size)
{
    for (int i = 0; i < partial_size; i++)
    {
        result[result_start_idx + i] = partial_res[i];
    }
}

int main(int argc, char **argv)
{

     // MPI initialization
    int rank, world_size;
    MPI_Init(NULL, NULL);
    MPI_Comm_size(MPI_COMM_WORLD, &world_size);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    // Open the file and check for successful opening
    ifstream test_file("../target.txt");
    if (!test_file.is_open()) {
        cout << "Failed to open test_file.txt" << endl;
        return -1;
    }

    // Read the file content into a string
    stringstream test_stream;
    test_stream << test_file.rdbuf();
    string test_string = test_stream.str();
    int target_length = test_string.length();
    char* target = new char[target_length + 1];
    strcpy(target, test_string.c_str());

    char* pattern = argv[1];
    int pattern_length = strlen(pattern);

    // Build KMP match table
    int *kmp_table = preKMP(pattern, pattern_length);

    // Calculate block sizes for pattern matching
    int block_overlap = pattern_length - 1;
    int block_per_proc_no_overlap = target_length / world_size;
    int block_per_proc = block_per_proc_no_overlap + ((world_size != 1) ? block_overlap : 0);
    int block_endProc = block_per_proc_no_overlap + target_length % world_size;

    int *result;
    int *partial_result;
    char *partial_target;

    // Master process handling
    if (rank == MASTER)
    {
        double ms = 0;
        auto start = std::chrono::high_resolution_clock::now();
        result = (int *)calloc(target_length, sizeof(int));
        partial_target = (char *)malloc(sizeof(char) * (block_per_proc + 1));
        strncpy(partial_target, target, block_per_proc);
        partial_target[block_per_proc] = '\0';
        partial_result = kmp(partial_target, pattern, kmp_table);

        mergePartialResultsIntoMain(result, 0, partial_result, block_per_proc);

        // Receive results from worker processes
        if (world_size > 1)
        {
            MPI_Request requests[world_size - 1];
            MPI_Status status[world_size - 1];

            int shift_idx = block_per_proc_no_overlap;
            for (int i = 1; i < world_size - 1; i++)
            {
                MPI_Irecv(result + shift_idx, block_per_proc, MPI_INT, i, 0, MPI_COMM_WORLD, &(requests[i - 1]));
                shift_idx += block_per_proc_no_overlap;
            }
            MPI_Irecv(result + shift_idx, block_endProc, MPI_INT, world_size - 1, 0, MPI_COMM_WORLD, &(requests[world_size - 2]));

            MPI_Waitall(world_size - 1, requests, status);
        }

        auto end = std::chrono::high_resolution_clock::now();

        // print result
        ms = std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count();

        int counter = 0;
        for (int i = 0; i < target_length; i++)
        {
            if (result[i] == 1)
                counter++;
        }

        cout <<" KMP_MPI algorithm result:(Target length; Pattern searched; Number of pattern found; Time used)"<<endl;
        cout << target_length << endl;
        cout << pattern << endl;
        cout << counter << endl;
        cout << ms << endl;
    }
    // Handling for the final worker process
    else if (rank == world_size - 1)
    {
        partial_target = (char *)malloc(sizeof(char) * (block_endProc + 1));
        int target_start_idx = block_per_proc_no_overlap * rank;
        strncpy(partial_target, target + target_start_idx, block_endProc);
        partial_target[block_endProc] = '\0';
        partial_result = kmp(partial_target, pattern, kmp_table);

        MPI_Request req;
        MPI_Isend(partial_result, block_endProc, MPI_INT, MASTER, 0, MPI_COMM_WORLD, &req);
    }
    // Handling for other worker processes
    else
    {
        partial_target = (char *)malloc(sizeof(char) * (block_per_proc + 1));
        int target_start_idx = block_per_proc_no_overlap * rank;
        strncpy(partial_target, target + target_start_idx, block_per_proc);
        partial_target[block_per_proc] = '\0';

        partial_result = kmp(partial_target, pattern, kmp_table);

        MPI_Request req;
        MPI_Isend(partial_result, block_per_proc, MPI_INT, MASTER, 0, MPI_COMM_WORLD, &req);
    }


    // Finalize MPI environment and free allocated memory
    free(partial_target);
    free(partial_result);
    free(kmp_table);
    if (rank == MASTER) {
        free(result); // Free result only in master
    }

    MPI_Finalize();
    return 0;
}

#!/usr/bin/env zsh
#SBATCH --job-name=print_task
#SBATCH --partition=instruction
#SBATCH --nodes=5 --cpus-per-task=20 --ntasks-per-node=2
#SBATCH --time=00:10:00
#SBATCH --mem=20G



module load mpi/mpich/4.0.2

mpicxx kmp_MPI.cpp -o kmp_mpi

srun -n 10 --cpu-bind=none ./kmp_mpi target

/*
 After finishing the code, I use Chat GPT to enhance the readability of my code by adding comments, improving variable naming, structuring the code for better readability, and segmenting the main function for clarity.
 */

#include <fstream>
#include <sstream>
#include <iostream>
#include <chrono>
#include <mutex>
#include <stdio.h>
#include <string.h>
#include <string>
#include <cstring>

using namespace std;
#define ALPHABET_SIZE 256

// Function to fill the bad character array, used in Boyer Moore's algorithm
void fillBadCharacterArray(char *pattern, int patternSize, int badChar[ALPHABET_SIZE]) {
    // Initialize all elements of badChar array to -1
    for (int i = 0; i < ALPHABET_SIZE; i++)
        badChar[i] = -1;

    // Fill the actual value of last occurrence of a character
    for (int i = 0; i < patternSize; i++)
        badChar[(int)pattern[i]] = i;
}

// Function to preprocess the pattern and fill the shift array and bpos array for strong good suffix rule
void preprocessStrongSuffix(int *shift, int *borderPos, char *pattern, int patternLength) {
    int i = patternLength, j = patternLength + 1;
    borderPos[i] = j;

    while (i > 0) {
        while (j <= patternLength && pattern[i - 1] != pattern[j - 1]) {
            if (shift[j] == 0)
                shift[j] = j - i;

            j = borderPos[j];
        }
        i--;
        j--;
        borderPos[i] = j;
    }

    int start = borderPos[0];
    for (int i = 0; i <= patternLength; i++) {
        if (shift[i] == 0)
            shift[i] = start;

        if (i == start)
            start = borderPos[j];
    }

}

// CUDA kernel function for the Boyer Moore algorithm
__global__ void boyerMooreKernel(const char* __restrict__ text, int textLen, const char* __restrict__ pattern, int patternLen, const int* __restrict__ delta1, const int* __restrict__ delta2,  int* matchPositions) {
    // Share memory
    extern __shared__ char sharedPattern[]; // Declare shared memory for the pattern
    if(blockDim.x<patternLen){
        int i = patternLen/blockDim.x;
        int remaining = patternLen%blockDim.x;
        for (int j =0; j<i ; j++){
            sharedPattern[threadIdx.x+j*blockDim.x] = pattern[threadIdx.x+j*blockDim.x];
        }
        if(threadIdx.x < remaining){
            sharedPattern[threadIdx.x+i*blockDim.x] = pattern[threadIdx.x+i*blockDim.x];
        }
    }
    if(threadIdx.x < patternLen){
        sharedPattern[threadIdx.x] = pattern[threadIdx.x];
     }
    __syncthreads();

    
    int tid = blockIdx.x * blockDim.x + threadIdx.x;
    int n = textLen / patternLen;
    if (tid < n) {
        int start = tid * patternLen;
        int end = min(start + (2 * patternLen), textLen);

        for (int i = start; i < end;) {
            int j = patternLen - 1;
            while (j >= 0 && (text[i + j] == sharedPattern[j])) {
                --j;
            }
            if (j < 0) {
                matchPositions[i + 1] = i + 1;
                break;
            } else
                i += max(delta1[j + 1], j - delta2[text[i + j]]);
        }
    }
}

int main(int argc, char *argv[]) {
    // Check if the correct number of arguments is provided
    if (argc != 3) {
        cout << "Incorrect number of arguments. Usage: <program> <pattern> <threads_per_block>" << endl;
        return -1;
    }

    // Open and read the target text file
    ifstream file("../target.txt");
    if (!file.is_open()) {
        cout << "Failed to open target file." << endl;
        return -1;
    }
    stringstream buffer;
    buffer << file.rdbuf();
    string fileContent = buffer.str();
    int fileLength = fileContent.length();
    char *targetText = new char[fileLength + 1];
    strcpy(targetText, fileContent.c_str());

    // Processing command-line arguments
    char *searchPattern = argv[1];
    int blockSize = atoi(argv[2]);
    int patternLength = strlen(searchPattern);

    // Allocate and initialize shift and border position arrays
    int *shiftArray = (int *)malloc(sizeof(int) * (patternLength + 1));
    memset(shiftArray, 0, sizeof(int) * (patternLength + 1));
    int *borderPosition = (int *)malloc(sizeof(int) * (patternLength + 1));

    // Allocate and initialize bad character and answer arrays
    int badCharacter[ALPHABET_SIZE];
    int *foundPositions = new int[fileLength];
    memset(foundPositions, -1, fileLength * sizeof(int));

    // Preprocessing steps
    preprocessStrongSuffix(shiftArray, borderPosition, searchPattern, patternLength);
    fillBadCharacterArray(searchPattern, patternLength, badCharacter);

    // Allocate memory on the GPU and copy data
    char *deviceText, *devicePattern;
    int *deviceShift, *deviceBadChar, *deviceFoundPositions;
    cudaMalloc((void **)&deviceText, fileLength * sizeof(char));
    cudaMalloc((void **)&devicePattern, patternLength * sizeof(char));
    cudaMalloc((void **)&deviceShift, (patternLength + 1) * sizeof(int));
    cudaMalloc((void **)&deviceBadChar, ALPHABET_SIZE * sizeof(int));
    cudaMalloc((void **)&deviceFoundPositions, fileLength * sizeof(int));

    cudaMemcpy(deviceText, targetText, fileLength * sizeof(char), cudaMemcpyHostToDevice);
    cudaMemcpy(devicePattern, searchPattern, patternLength * sizeof(char), cudaMemcpyHostToDevice);
    cudaMemcpy(deviceShift, shiftArray, (patternLength + 1) * sizeof(int), cudaMemcpyHostToDevice);
    cudaMemcpy(deviceBadChar, badCharacter, ALPHABET_SIZE * sizeof(int), cudaMemcpyHostToDevice);
    cudaMemcpy(deviceFoundPositions, foundPositions, fileLength * sizeof(int), cudaMemcpyHostToDevice);

    // Calculate the number of blocks needed
    int numBlocks = fileLength / patternLength / blockSize + ((fileLength / patternLength) % blockSize == 0 ? 0 : 1);

    // Initialize CUDA events for timing
    cudaEvent_t start, stop;
    cudaEventCreate(&start);
    cudaEventCreate(&stop);
    cudaEventRecord(start);

    // Launch the Boyer Moore CUDA kernel
    boyerMooreKernel<<<numBlocks, blockSize,patternLength* sizeof(char)>>>(deviceText, fileLength, devicePattern, patternLength, deviceShift, deviceBadChar, deviceFoundPositions);

    // Wait for GPU operations to complete and measure time
    cudaEventRecord(stop);
    cudaEventSynchronize(stop);
    float elapsedTime;
    cudaEventElapsedTime(&elapsedTime, start, stop);
    cudaMemcpy(foundPositions, deviceFoundPositions, fileLength * sizeof(int), cudaMemcpyDeviceToHost);

    // Count the number of pattern matches
    int matchCount = 0;
    for (int i = 0; i < fileLength; i++) {
        if (foundPositions[i] != -1) {
            matchCount++;
        }
    }

    // Output the results
    cout << "BM_CUDA algorithm(Global Memory) result: (Target length; Pattern length; Number of patterns found; Time used(ms)" << endl;
    cout << fileLength << endl;
    cout << patternLength << endl;
    cout << matchCount << endl;
    cout << elapsedTime << endl;

    // Free memory
    cudaFree(deviceText);
    cudaFree(devicePattern);
    cudaFree(deviceShift);
    cudaFree(deviceBadChar);
    cudaFree(deviceFoundPositions);
    delete[] targetText;
    delete[] foundPositions;

    return 0;
}


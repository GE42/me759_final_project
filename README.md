## Fall 2023: ME759 Final Project

This repository includes all the essential files required for the final project of the ME 759 course at the University of Wisconsin-Madison. The project is centered on the development and optimization of the **String Matching Algorithm**, implementing it across various parallel architectures.

## Quick Introduction: 
- **Name:** Yuanjun Ge
- **Email:** ge42@wisc.edu
- **Home Department:** Computer Sciences
- **Status:** Master Student
- **Name of your teammate (if applicable):** Jiahui Yang

## Project Title: 
Parallel String Matching Algorithm

## Link to git repo for project: 
- https://git.doit.wisc.edu/GE42/me759_final_project

## Problem Statement: 
The objective of this project is to address the computational challenge of string pattern matching by parallelizing two renowned algorithms, the **Knuth-Morris-Pratt (KMP)** and **Boyer-Moore (BM)**. The focus is to develop implementations that exploit the parallel architectures of both CPUs and GPUs. By integrating OpenMP and MPI for CPU-based parallelism and CUDA for GPU-based execution, we strive to compare and enhance the performance of these algorithms in finding pattern occurrences in large text corpuses. The success of this project lies in the effective utilization of each platform's strengths, thereby reducing the overall computation time and improving efficiency in real-world applications.

## Project Features:
- **Knuth–Morris–Pratt String Matching Algorithm**
  - CPU
  - OpenMP
  - MPI
  - CUDA
    - Global Memory
    - Shared Memory
  
- **Boyer-Moore String Matching Algorithm**
  - CPU
  - OpenMP
  - MPI
  - CUDA
    - Global Memory
    - Shared Memory

## Project Structure:
- `target.txt`: A large file with a size of approximately 200 MiB, containing a total of 209,715,200 strings, each consisting of characters ranging from 'a' to 'z'. This file is specifically structured for performance testing of the string matching algorithms. It includes:
  - 20 occurrences of the pattern 'testtesttestxyz'.
  - 32 occurrences of the pattern 'target'.
  - 5 occurrences of the pattern 'thisisalongsentence'.


- `test.txt`: A relatively small file, approximately 1 MB in size, used primarily for code execution and testing purposes.


- `Proposal.pdf`: This repository contains our proposal.


- `/fig`: This repository contains all the output figures.


- `/KMP_CPU`: This repository contains the implementation of the Knuth-Morris-Pratt (KMP) string matching algorithm using CPU.
  - `kmp_cpu.cpp`: The C++ source code for the KMP algorithm with CPU.
  - `kmp_cpu.sh:`: This SLURM script compiles and executes a C++ program implementing the KMP string matching algorithm.
    - Compile: ```g++ kmp_cpu.cpp -Wall -O3 -std=c++17 -o kmp_cpu```
    - Execute: ./kmp_cpu [Pattern]
    - Example: ```./kmp_cpu target```


- `/KMP_CUDA`: This repository contains the implementation of the Knuth-Morris-Pratt (KMP) string matching algorithm using CUDA with various optimization techniques, such as the use of global and shared memory .
  - `kmp_CUDA_Global.cu`: A CUDA file containing the implementation of the Knuth-Morris-Pratt (KMP) algorithm with global memory usage. This implementation is optimized for performance using CUDA, which allows for parallel processing on NVIDIA GPUs.
    - Compile: ```nvcc kmp_CUDA_Global.cu -Xcompiler -O3 -Xcompiler -Wall -Xptxas -O3 -std c++17 -o kmp_cuda```
    - Execute: ./kmp_cuda [Pattern] [Number of Threads Per Block]
    - Example: ```./kmp_cuda target 512```
  - `kmp_CUDA_Shared.cu`: Another CUDA file, which is presumably an alternate implementation of the KMP algorithm utilizing shared memory in CUDA. This version is likely focused on optimizing memory usage and access patterns to enhance performance on GPUs.
    - Compile: ```nvcc kmp_CUDA_Shared.cu -Xcompiler -O3 -Xcompiler -Wall -Xptxas -O3 -std c++17 -o kmp_cuda```
    - Execute: ./kmp_cuda [Pattern] [Number of Threads Per Block]
    - Example: ```./kmp_cuda target 512```
  - `kmp_CUDA_slurm.sh:`: A shell script designed for submitting jobs to a Slurm workload manager.
  - `slurm-*.out:`: Output files from the Slurm, documenting the test time of the results.


- `/KMP_MPI`: This repository contains the implementation of the Knuth-Morris-Pratt (KMP) string matching algorithm, parallelized using MPI.
  - `kmp_MPI.cpp`: C++ source file implementing the KMP algorithm using the Message Passing Interface (MPI). This implementation is aimed at distributed memory systems, allowing the algorithm to run on multiple processors in parallel.
    - Compile: ```mpicxx kmp_MPI.cpp -o kmp_mpi```
    - Execute: srun -n [Number of Processors] --cpu-bind=none ./kmp_mpi [pattern]
    - Example: ```srun -n 10 --cpu-bind=none ./kmp_mpi target```
  - `kmp_MPI_slurm.sh:`: A shell script designed for submitting jobs to a Slurm workload manager.
  - `slurm-*.out:`: Output files from the Slurm, documenting the test time of the results.


- `/KMP_OpenMP`: This repository contains the implementation of the Knuth-Morris-Pratt (KMP) string matching algorithm, parallelized using OpenMP.
  - `/sample_code`: The folder contains a OpenMP implementation of the KMP algorithm (kmp_omp.cpp) and its accompanying shell script (kmp_omp.sh) for execution with set configurations on a GPU-enhanced cluster environment.
    - `kmp_omp.cpp`: C++ source file implementing the KMP algorithm using OpenMP.
    - `kmp_omp.sh:`: A shell script designed for submitting jobs to a Slurm workload manager.
      - Compile: ```g++ kmp_omp.cpp -Wall -O3 -std=c++17 -o kmp_omp -fopenmp```
      - Execute: ./kmp_omp [Pattern]
      - Example: ```./kmp_omp target```
  - `kmp_all_jobsubmit.sh`: This script automates the process of creating directories numbered 1 to 10, copying specific files into each, modifying a shell script with the current directory number, and submitting each version as a job to the SLURM scheduler.
  - `kmp_all_re.sh`: This script checks for the existence of "result.txt", creates a fresh file if necessary, then loops through directories numbered 1 to 10 to extract elapsed times from output files and appends them to a central "result.txt" file for aggregation.


- `/BM_CPU`: This repository contains the implementation of the Boyer-Moore (BM) String Matching Algorithm using CPU.
  - `bm_cpu.cpp`: The C++ source code for the BM algorithm with CPU.
  - `bm_cpu.sh:`: This SLURM script compiles and executes a C++ program implementing the BM string matching algorithm.
    - Compile: ```g++ bm_cpu.cpp -Wall -O3 -std=c++17 -o bm_cpu -fopenmp```
    - Execute: ./bm_cpu [Pattern]
    - Example: ```./bm_cpu target```


- `/BM_CUDA`: This repository contains the implementation of the Boyer-Moore (BM) string matching algorithm using CUDA with various optimization techniques, such as the use of global and shared memory .
  - `bm_CUDA_Global.cu`: BM algorithm implementation using CUDA global memory.
    - Compile: ```nvcc bm_CUDA_Global.cu -Xcompiler -O3 -Xcompiler -Wall -Xptxas -O3 -std c++17 -o bm_cuda```
    - Execute: ./bm_cuda [Pattern] [Number of Threads Per Block]
    - Example: ```./bm_cuda target 512```
  - `bm_CUDA_Shared.cu`: BM algorithm implementation using CUDA shared memory.
    - Compile: ```nvcc bm_CUDA_Shared.cu -Xcompiler -O3 -Xcompiler -Wall -Xptxas -O3 -std c++17 -o bm_cuda```
    - Execute: ./bm_cuda [Pattern] [Number of Threads Per Block]
    - Example: ```./bm_cuda target 512```
  - `bm_CUDA_slurm.sh:`: A shell script designed for submitting jobs to a Slurm workload manager.
  - `slurm-*.out:`: Output files from the Slurm, documenting the test time of the results. 


- `/BM_MPI`: This repository contains the implementation of the Boyer-Moore (BM) string matching algorithm, parallelized using MPI.
  - `/sample_code`: The folder contains a MPI implementation of the BM algorithm (bm_mpi.cpp) and its accompanying shell script (bm_mpi.sh) for execution with set configurations on a GPU-enhanced cluster environment.
    - `bm_mpi.cpp`: The C++ source code for the BM algorithm with MPI using different numbers of CPUs.
    - `bm_mpi.sh:`: A shell script designed for submitting jobs to a Slurm workload manager.
      - Compile: ```mpicxx bm_mpi.cpp -Wall -O3 -std=c++17 -o bm_mpi -fopenmp -lgomp```
      - Execute: srun -n [Number of Processors] --cpu-bind=none ./bm_mpi target.txt [pattern]
      - Example: ```srun -n 2 --cpu-bind=none ./bm_mpi target.txt target```
  - `/numProcessor`: 
    - `bm_mpi.cpp`: The C++ source code for the BM algorithm with MPI using different numbers of CPUs.
    - `bm_mpi.sh`: This SLURM script compiles and executes a C++ program implementing the BM string matching algorithm.
    - `bm_all_jobsubmit.sh`: This script automates the process of creating directories numbered 1 to 10, copying specific files into each, modifying a shell script with the current directory number, and submitting each version as a job to the SLURM scheduler.
    - `bm_all_re.sh`: This script checks for the existence of "result.txt", creates a fresh file if necessary, then loops through directories numbered 1 to 10 to extract elapsed times from output files and appends them to a central "result.txt" file for aggregation.
  - `/numThread`:
    - `bm_mpi.cpp`: The C++ source code for the BM algorithm with MPI using different numbers of threads.
    - `bm_mpi.sh`: This SLURM script compiles and executes a C++ program implementing the BM string matching algorithm.
    - `bm_all_jobsubmit.sh`: This script automates the process of creating directories numbered 1 to 10, copying specific files into each, modifying a shell script with the current directory number, and submitting each version as a job to the SLURM scheduler.
    - `bm_all_re.sh`: This script checks for the existence of "result.txt", creates a fresh file if necessary, then loops through directories numbered 1 to 10 to extract elapsed times from output files and appends them to a central "result.txt" file for aggregation.


- `/BM_OpenMP`: This repository contains the implementation of the Boyer-Moore (BM) string matching algorithm, parallelized using OpenMP.
  - `/sample_code`: The folder contains a OpenMP implementation of the BM algorithm (bm_omp.cu) and its accompanying shell script (bm_omp.sh) for execution with the set number of threads.
    - `bm_omp.cpp`: C++ source file implementing the BM algorithm using OpenMP.
    - `bm_omp.sh:`: A shell script designed for submitting jobs to a Slurm workload manager.
      - Compile: ```g++ bm_omp.cpp -Wall -O3 -std=c++17 -o bm_omp -fopenmp```
      - Execute: ./bm_omp target.txt [pattern]
      - Example: ```./bm_omp target.txt target```
  - `bm_all_jobsubmit.sh`: This script automates the process of creating directories numbered 1 to 10, copying specific files into each, modifying a shell script with the current directory number, and submitting each version as a job to the SLURM scheduler.
  - `bm_all_re.sh`: This script checks for the existence of "result.txt", creates a fresh file if necessary, then loops through directories numbered 1 to 10 to extract elapsed times from output files and appends them to a central "result.txt" file for aggregation.


## Acknowledgments:

We would like to acknowledge the assistance provided by OpenAI's ChatGPT in formulating parts of this project.


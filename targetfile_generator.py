import random
import string

def insert_randomly(base_string, pattern, count):
    """Inserts a given pattern into the base string at random positions, count times."""
    for _ in range(count):
        position = random.randint(0, len(base_string))
        base_string = base_string[:position] + pattern + base_string[position:]
    return base_string

def main():
    file_size = 200 * 1024 * 1024
    pattern1 = "testtesttestxyz"
    pattern2 = "target"
    pattern3 = "thisisalongsentence"
    count1 = 20
    count2 = 30
    count3 = 5

    total_pattern_size = (len(pattern1) * count1) + (len(pattern2) * count2) + (len(pattern3) * count3)
    adjusted_size = file_size - total_pattern_size

    # Generate the base random string
    random_string = ''.join(random.choices(string.ascii_lowercase, k=adjusted_size))

    # Insert each pattern randomly
    random_string_with_patterns = insert_randomly(random_string, pattern1, count1)
    random_string_with_patterns = insert_randomly(random_string_with_patterns, pattern2, count2)
    random_string_with_patterns = insert_randomly(random_string_with_patterns, pattern3, count3)

    if len(random_string_with_patterns) > file_size:
        random_string_with_patterns = random_string_with_patterns[:file_size]

    file_path = 'target.txt'
    with open(file_path, 'w') as file:
        file.write(random_string_with_patterns)

    print(f"File created: {file_path}")

if __name__ == "__main__":
    main()

// kmp_omp.cpp

/*After completing my test,
I requested suggestions from ChatGPT
for improving the code.*/

/* This C++ program, kmp_omp.cpp, implements the Knuth-Morris-Pratt (KMP) 
string matching algorithm with parallel processing using OpenMP. 
It reads a text file and a pattern from input, then efficiently searches for 
occurrences of the pattern within the text. The program utilizes multiple cores 
specified by an environment variable (CORE_COUNT) or a default value (6) if not set. 
It measures the elapsed time for the matching process and outputs the text and 
pattern lengths, the number of cores used, the elapsed time, and the total matches found. */

#include <ctime>
#include <iostream>
#include <string>
#include <cstring>
#include <fstream>
#include <sstream>
#include <omp.h>  // Include OpenMP for parallel processing
#include <cstdlib> // Include for getenv

// Global variable for core count
int CORE_COUNT;

using namespace std;

// Function prototypes
void ComputeFailureArray(char *pattern, int failure[]);
void KMPMatch(char *text, char *pattern, int *failure, int *matchLocations, int patternLength, int textLength, int startIdx);

// Main function of the program
int main(int argc, char **argv)
{
     // Get CORE_COUNT value from the environment variable
    const char* envCoreCount = getenv("coreCount");
    if (envCoreCount != nullptr) {
        CORE_COUNT = std::stoi(envCoreCount);
    } else {
        CORE_COUNT = 6; // Default value if not set
    }

    // Check for correct command-line arguments
    if (argc < 3) {
        cerr << "Usage: " << argv[0] << "<pattern> <num_threads>" << endl;
        return 1;
    }

    // ifstream object for reading from the text file
    ifstream textFileStream("target.txt");
    stringstream textStringStream;

    // Reading the content of the text file into a string stream
    textStringStream << textFileStream.rdbuf();
    string textContent = textStringStream.str();

    // Use the second command line argument as the pattern string
    string patternContent = argv[1];
    // Number of threads for parallel processing is taken from the command line
    int numThreads = stoi(argv[2]);

    // Determine the lengths of the text and pattern strings
    int textLength = textContent.length();
    int patternLength = patternContent.length();

    // Dynamic memory allocation for text and pattern character arrays
    char *text = new char[textLength + 1];
    char *pattern = new char[patternLength + 1];

    // Copying the contents of strings to character arrays
    strcpy(text, textContent.c_str());
    strcpy(pattern, patternContent.c_str());

    // Arrays to store the failure function and the positions of matches found
    int *failureArray = new int[patternLength];
    int *matchLocations = new int[textLength]();

    // Compute the failure function array for the pattern
    ComputeFailureArray(pattern, failureArray);

    // Variables for timing the algorithm
    timespec startTime, endTime;
    double elapsedTime;

    // Start the timer for performance measurement
    cout << "Parallel KMP Algorithm Results" << endl;
    clock_gettime(CLOCK_MONOTONIC, &startTime);

    // Divide the text into equal parts for parallel processing
    int partitionLength = textLength / CORE_COUNT;

    // Parallel section utilizing OpenMP
    #pragma omp parallel num_threads(numThreads)
    {
        // Loop to perform KMP algorithm on each part of the text
        #pragma omp for
        for (int i = 0; i < CORE_COUNT; i++)
        {
            int currentStartIdx = i * partitionLength;
            KMPMatch(text, pattern, failureArray, matchLocations, patternLength, partitionLength, currentStartIdx);
        }

        // Handling potential overlaps between divided text parts
        int overlapLength = (patternLength - 1) * 2;

        #pragma omp for
        for (int i = 0; i < CORE_COUNT - 1; i++)
        {
            int overlapStartIdx = (i + 1) * partitionLength - (patternLength - 1);
            KMPMatch(text, pattern, failureArray, matchLocations, patternLength, overlapLength, overlapStartIdx);
        }
    }

    // Processing the last section of the text, accounting for any remainder in division
    int remainingLength = (textLength % CORE_COUNT) + patternLength - 1;
    if (remainingLength != 0)
    {
        int lastStartIdx = CORE_COUNT * partitionLength - (patternLength - 1);
        KMPMatch(text, pattern, failureArray, matchLocations, patternLength, remainingLength, lastStartIdx);
    }

    // Stop the timer and calculate the elapsed time
    clock_gettime(CLOCK_MONOTONIC, &endTime);
    elapsedTime = (endTime.tv_sec - startTime.tv_sec) * 1e3 + (endTime.tv_nsec - startTime.tv_nsec) / 1e6;

    // Output the results: lengths of the text and pattern, and the elapsed time
    cout << "Text Length: " << textLength << endl;
    cout << "Pattern Length: " << patternLength << endl;
    cout << "Number of Cores: " << CORE_COUNT << endl;
    cout << "Elapsed Time: " << elapsedTime << " ms" << endl;

    // Count the total number of pattern matches found in the text
    int totalMatches = 0;
    for (int i = 0; i < textLength; i++)
    {
        if (matchLocations[i])
        {
            totalMatches++;
        }
    }

    // Display the total number of matches found
    cout << "Total Matches Found: " << totalMatches << endl;

    // Release the dynamically allocated memory
    delete[] text;
    delete[] pattern;
    delete[] failureArray;
    delete[] matchLocations;

    return 0;
}

// Compute the failure function for KMP algorithm
void ComputeFailureArray(char *pattern, int failure[])
{
    int patternSize = strlen(pattern);
    failure[0] = -1;  // The first element of failure array is always -1

    // Iterating over the pattern to build the failure function
    for (int i = 1; i < patternSize; i++)
    {
        int k = failure[i - 1];
        while (k >= 0)
        {
            // Checking if there is a suffix which is also a prefix
            if (pattern[k] == pattern[i - 1])
                break;  // If found, break the loop
            else
                k = failure[k];  // Move to the next best candidate
        }
        failure[i] = k + 1;  // Set the failure position for the current index
    }
    return;
}

// Perform the Knuth-Morris-Pratt string matching algorithm
void KMPMatch(char *text, char *pattern, int *failure, int *matchLocations, int patternLength, int textLength, int startIdx)
{
    int i = startIdx, j = startIdx + textLength;
    int k = 0;  // Index for traversing the pattern

    // Loop through the text to find matches
    while (i < j)
    {
        if (k == -1)
        {
            i++;  // If k is -1, move to the next character in text and reset k
            k = 0;
        }
        else if (text[i] == pattern[k])
        {
            i++;  // If characters match, increment both indices
            k++;
            if (k == patternLength)  // Check if the whole pattern has been matched
            {
                k--;  // Adjust the index
                matchLocations[i - patternLength] = 1;  // Mark the match position
                i = i - patternLength + 1;  // Move to the next potential match start
            }
        }
        else
        {
            k = failure[k];  // Use the failure function to skip unnecessary comparisons
        }
    }
    return;
}






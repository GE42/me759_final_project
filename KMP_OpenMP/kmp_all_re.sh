#!/bin/bash

# Check if a file named "result.txt" exists in the current directory
if [ -f "result.txt" ]; then
    # If the file exists, delete it to start with a fresh file
    rm "result.txt"
fi

# Create a new, empty file named "result.txt"
touch "result.txt"

# Loop through numbers from 1 to 10
for n in $(seq 1 10)  
do 
    # Create a directory named after the current number (n) 
    # or move into it if it already exists
    DIR="${n}"
    cd "$DIR"

    # Extract the elapsed time from the file 'kmp_omp.out'
    # 'sed -n '5p'' selects the fifth line of the file
    # 'grep -oP' then extracts the numeric value of the elapsed time using a Perl-compatible regex
    # The result (elapsed time in milliseconds) is appended to "result.txt" in the specified directory
    sed -n '5p' kmp_omp.out | grep -oP '(?<=Elapsed Time: ).*(?= ms)' >> "../result.txt"

    # Change back to the parent directory after processing the current directory
    cd ..

done  # End of the loop for numbers 1 to 10






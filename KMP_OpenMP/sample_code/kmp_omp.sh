#!/bin/bash

# Set SLURM job parameters
#SBATCH --job-name=kmp_omp         # Job name
#SBATCH --partition=instruction    # Specify the partition or queue
#SBATCH --time=00-00:30:00         # Runtime in Days-Hours:Minutes:Seconds (30 minutes here)
#SBATCH --ntasks=1                 # Number of tasks (jobs), typically 1 for serial jobs
#SBATCH --cpus-per-task=4          # Number of CPU cores per task (4 in this case)
#SBATCH --mem=20G                  # Memory needed per job (20 gigabytes here)
#SBATCH --output=kmp_omp.out       # Name of the output file (stdout and stderr)

cd $SLURM_SUBMIT_DIR               # Change directory to the directory where the job was submitted

# Compile your C++ program with OpenMP support
g++ kmp_omp.cpp -Wall -O3 -std=c++17 -o kmp_omp -fopenmp

# Execute your program with appropriate arguments
./kmp_omp target




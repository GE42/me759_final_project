#!/bin/bash

#SBATCH --job-name=kmp_omp
#SBATCH --partition=instruction
#SBATCH --time=00-00:30:00
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=4
#SBATCH --mem=20G
#SBATCH --output=kmp_omp.out

# Change to the directory from where the script was submitted
cd $SLURM_SUBMIT_DIR

# Set the number of cores using the environment variable {n}
export coreCount={n}

# Compile your C++ program with OpenMP support
g++ kmp_omp.cpp -Wall -O3 -std=c++17 -o kmp_omp -fopenmp

# Execute your program with the specified number of threads
./kmp_omp target $coreCount




